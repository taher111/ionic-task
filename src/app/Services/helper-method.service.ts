import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable()
export class HelperMethodsProvider {
  loading;
  dismiss;
  constructor(private loadingCtrl: LoadingController) {}

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({
      cssClass: 'custom-spinner',
    });
    await this.loading.present();
  }

  dismissLoading() {
    this.loading
      .dismiss()
      .then((res) => {
        console.log('Loading dismissed!', res);
      })
      .catch((error) => {
        console.log('error', error);
      });
  }
}
