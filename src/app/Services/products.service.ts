import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  url: any;
  constructor(public http: HttpClient) {
    this.url = 'http://tindiostag.tindio.com/api/home/';
  }
  getAllProducts(count): Observable<any> {
    return this.http.get(`${this.url}?items_per_page=24&page=${count}`);
  }
}
