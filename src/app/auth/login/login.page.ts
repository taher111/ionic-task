import { Component, OnInit } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage implements OnInit {
  userData: any;
  constructor(public route: Router, private googlePlus: GooglePlus) {}
  ngOnInit() {}
  navigateToHome() {
    const x = this.googlePlus.getSigningCertificateFingerprint();
    console.log(' x : ', x);

    this.googlePlus
      .login({})
      .then((result) => {
        this.userData = result;
        console.log('login with google response : ', result);
        // this.route.navigate(['/home']);
      })
      .catch((err) => {
        // tslint:disable-next-line: new-parens
        throw new Error(`Error ${JSON.stringify(err)}`);
      });
    this.route.navigate(['/home']);
  }
}
