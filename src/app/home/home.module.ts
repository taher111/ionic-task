import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { PricePipe } from '../price-pipe.pipe';
import { HelperMethodsProvider } from '../Services/helper-method.service';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, HomePageRoutingModule],
  declarations: [HomePage, PricePipe],
  providers: [HelperMethodsProvider],
})
export class HomePageModule {}
