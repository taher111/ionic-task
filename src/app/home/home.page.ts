import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../Services/products.service';
import { Router } from '@angular/router';
import { HelperMethodsProvider } from '../Services/helper-method.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  items: any;
  count = 0;
  constructor(
    public productService: ProductsService,
    public spinner: HelperMethodsProvider,
    public router: Router
  ) {}

  ngOnInit() {
    this.spinner.presentLoading();
    this.productService.getAllProducts(this.count).subscribe(
      (data: any) => {
        this.items = data.list;
        this.spinner.dismissLoading();
        console.log(data);
      },
      (err) => {
        if (!navigator.onLine) {
          alert('You are offline , please check your internet connection !');
          this.spinner.dismissLoading();
        } else {
          this.spinner.dismissLoading();
          alert('Something went wrong , Please try again later!');
        }
      }
    );
  }
  getMoreProducts(event) {
    let products = [];
    this.count++;
    this.productService.getAllProducts(this.count).subscribe(
      (data: any) => {
        products = data.list;
        this.items.push(...products);
        console.log(this.items);
      },
      (error) => {
        if (!navigator.onLine) {
          alert('You are offline , please check your internet connection !');
        } else {
          alert('Something went wrong , Please try again later!');
        }
      }
    );
    event.target.complete();
  }
  navigateToLoginPage() {
    this.router.navigate(['/login']);
  }
}
