import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Pipe({ name: 'pricePipe' })
export class PricePipe implements PipeTransform {
  transform(value: any, args: string) {
    const pipe = new DecimalPipe('en');
    value = isNaN(value) ? value.toString().replace(',', '') : +value;
    return pipe.transform(value, args);
  }
}
